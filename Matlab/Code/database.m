function [db] = database(type)
    badImages = {
        '9ac38cd713c248058faf.png'
        'iJA4JZ4AX7GZ001577_1.jpg'      %
        'i2HGFC2F76GH555141_1.jpg'
        'i19XFB2F82FE236436_1.jpg'
        'i3GCPCREC2FG462822_1.jpg'
        'i2HGFG4A59FH700841_1.jpg'      %
        'i1C4BJWEG0DL600746_1.jpg'
        '1e8a7c77c695475dab00.jpg'
        'ec41d4a0cbb9438ca7a0.jpg'
        'a4b7444069de462caeae.jpg'
        '10ff279141824736a350.jpg'
        'i19UYA42661A011588_1.jpg'      %
        'iJA4AD3A35GZ057152_1.jpg'
        'iWAUFGAFCXEN161251_1.jpg'
        'iWAUE8HFF9G1096687_1.jpg'
        'i1FADP3K2XFL329248_1.jpg'
        'i1FBAX2CM3HKA90484_1.jpg'
        'i2T3BFREV3FW402020_1.jpg'
        'i2HGFC2F77GH523718_1.jpg'
        'i5FNYF5H85GB020975_1.jpg'
        'iJA32U2FU9GU002057_1.jpg'
		'i1FMJU1HT6HEA08711_1.jpg'
        };
    goodImage = {
        'i19XFB4F20EE201366_1.jpg'
        'i2HKYF18557H520439_1.jpg'
        'iJA4JZ4AX7GZ001577_5.jpg'
        'iJA4AD3A35GZ057152_2.jpg'
        'iJA4AD3A35GZ057152_3.jpg'
        'i2GNFLHEK7F6174651_9.jpg'
        'i1GCVKREC2FZ342741_5.jpg'
        'i1GCNKREC4GZ101670_1.jpg'
        'iJHLRE38359C005136_2.jpg'
        'i2HKRM3H54GH565796_1.jpg'
        'i2HKRM3H54GH565796_2.jpg'
        'i2HKRM3H57GH510680_12.jpg'
        'i2HKRW1H80HH514273_9.jpg'
        };
    db = [];
    if(type == "bad")
        db = badImages;
    elseif(type == "good")
        db = goodImage;
    end
end