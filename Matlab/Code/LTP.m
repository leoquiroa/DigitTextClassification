% function [newC] = LTP(I,n)
    n = 1;
    I = imread('..\images\bad\i1FMJU1HT6HEA08711_1.jpg');
    if (size(I,3) == 3) 
        I = rgb2gray(I); 
    end
    
    [r,c] = size(I);
    R = zeros(r,c,8);
    thr = 5;
    
    I = double(I);
    M = createMask(n);
    
    for i=1:8    
       R(:,:,i) = imfilter(I,M(:,:,i));    
    end
    % get the code bits
    E = abs(R) > thr;
    P = (R <= 0).*(E);
    N = (R >= 0).*(E);
    % convert to the code
    C = zeros(r,c,2);
    for i=1:8
       C(:,:,1) = C(:,:,1) + P(:,:,i).*2^(i-1);
       C(:,:,2) = C(:,:,2) + N(:,:,i).*2^(i-1);
    end
    newC = [C(:,:,1) C(:,:,2)];
% end

function D = createMask(n)
    D = zeros(2*n+1,2*n+1,8);
    % give the center negative value
    D(n+1,n+1,:) = -1; 
    % each of the eight neighbors
    D(n+1,2*n+1,1) = 1;
    D(1,2*n+1,2) = 1;
    D(1,n+1,3) = 1;
    D(1,1,4) = 1;
    D(n+1,1,5) = 1;
    D(2*n+1,1,6) = 1;
    D(2*n+1,n+1,7) = 1;
    D(2*n+1,2*n+1,8) = 1;
end
