% function [newC] = LDN(I,n)
    n = 1;
    I = imread('bad_0204\i1FMJU1HT6HEA08711_1.jpg');
    if (size(I,3) == 3) 
        I = rgb2gray(I); 
    end
    [r,c] = size(I);
    R = zeros(r,c,8);
    
    
    
    I = double(I);
    M = createMask(n);
    
    for i=1:8    
       R(:,:,i) = imfilter(I,M(:,:,i));    
    end
    [~,mapD] = sort(abs(R),3,'descend'); %order the magnitude without sign
    top = mapD(:,:,1:2)-1; %get the two prominent
    Dp = top(:,:,1);
    Ds = top(:,:,2);
    oldC = Dp.*8 + Ds; 
    %000 + 000
    %32,16,8 + 4,2,1
    filter = [1 2 3 4 5 6 7 8 10 11 12 13 14 15 16 17 19 20 21 22 23 24 25 26 28 29 30 31 32 33 34 35 37 38 39 40 41 42 43 44 46 47 48 49 50 51 52 53 55 56 57 58 59 60 61 62];
    filter = filter + 1; % shifted to start at 1
    lut = zeros(56,1);
    lut(filter) = 1:length(filter);
    newC = lut(oldC+1);
% end

function D = createMask(n)
    D = zeros(2*n+1,2*n+1,8);
    % give the center negative value
    D(n+1,n+1,:) = -1; 
    % each of the eight neighbors
    D(n+1,2*n+1,1) = 1;
    D(1,2*n+1,2) = 1;
    D(1,n+1,3) = 1;
    D(1,1,4) = 1;
    D(n+1,1,5) = 1;
    D(2*n+1,1,6) = 1;
    D(2*n+1,n+1,7) = 1;
    D(2*n+1,2*n+1,8) = 1;
end
